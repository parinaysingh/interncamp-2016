<?php
	session_start();
	require_once('db_config.php');
	if(!isset($_SESSION["cvlogged"])){
		header('Location: /dashboard/loginpage.php');
		exit();
	}
	$cvlogged = $_SESSION['cvlogged'];	
	if($stmt = $conn->prepare("SELECT cvid FROM internapply WHERE id = ?")){
		$stmt->bind_param('i',$cvlogged);	
		$stmt->execute();
		$stmt->store_result();
		$stmt->bind_result($cvid);
		$stmt->fetch();
		if($cvid){
				$_SESSION['loggedid'] = $cvlogged;
				header('Location: /dashboard/index.php');
				exit();
			}
	}else{ 
		echo "Error";
		exit();
	}
		
?>
<!doctype html>
<html>
<head>
	<link rel="icon" href="img/icamp.png">
	<title>Upload Resume</title>
	<style>
	div.upload {
    width: 210px;
    height: 77px;
    background: url(img/resumeup.png);
    background-size: cover;
    overflow: hidden;
    margin-left: 40%;
    display: block;
}
div.upload input {
    display: block !important;
    width: 100% !important;
    height: 57px !important;
    opacity: 0 !important;
    overflow: hidden !important;
}
div.Send {
    width: 230px;
    height: 70px;
    background: url(img/next.png);
    background-size: cover;
    overflow: hidden;
    margin-left: 41%;
    display: block;
}
div.Send input {
    display: block !important;
    width: 100% !important;
    height: 57px !important;
    opacity: 0 !important;
    overflow: hidden !important;
}
	</style>
	</head>

	<body>
		<form enctype="multipart/form-data" action="cvupload.php" method="POST">
		    <div class="upload">
		    <input name="file" type="file" />
		</div>
		<div class="Send">
		    <input type="submit" value="Send File" />
		    <div>
		</form>
	</body>
</html>
