<?php
	session_start();
	require_once('db_config.php');
	
	if(isset($_POST['password'])){
		$cvlogged = $_SESSION['cvlogged'];
		$pass = $conn->real_escape_string($_POST['password']);
		$storepass = password_hash($pass, PASSWORD_BCRYPT, array('cost' => '10'));
		$insert="UPDATE internapply SET password = ? WHERE id = ?";
		if($stmt=$conn->prepare($insert)){
    		$stmt->bind_param("si",$storepass, $cvlogged);
    		$stmt->execute();
    		$stmt->close();
			$_SESSION['loggedid'] = $cvlogged;
			header('Location: /dashboard/index.php');
			exit();
    	}else{
			echo "Failure";
    		var_dump($conn->error);
    	}
	}else{
		echo "Required Field Missing";
	}
?>