<!doctype html>
<?php 
	session_start();
	require_once('db_config.php');
			
	if(!isset($_SESSION["loggedid"])){
		header('Location: /dashboard/loginpage.php');
		exit();
	}
	$loggedid =$_SESSION["loggedid"];
	
?>
<html lang="en" class="no-js">
<head>
	<link rel="icon" href="img/icamp.png">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="css/reset.css"> 
	<link rel="stylesheet" href="css/style.css"> 
	<script src="js/modernizr.js"></script> 
	 <link rel="stylesheet" href="css/main.css"> 	
	<title>Dashboard</title>
	<style>
		.container{
			width:250px;
			height:100px;
			border-radius:3px;
			background:#333;
			color:#FFF;
			padding:5px;
			float:left;
			margin: 0 5px 10px 5px;
		}
	</style>
</head>
<body>
	<header class="cd-main-header">
		<a href="#0" class="cd-logo"><img src="img/icamp.png" alt="Logo" height="65px" width="90px"></a>
		<a href="#0" class="cd-nav-trigger">Menu<span></span></a>
		<nav class="cd-nav">
			<ul class="cd-top-nav">
				<li class="has-children account">
					<a href="#0">
						Account
					</a>
					<ul>
						<li><a href="#0" onClick="logout()">Logout</a></li>
					</ul>
				</li>
			</ul>
		</nav>
	</header> 
	<main class="cd-main-content">
		<nav class="cd-side-nav">
			<ul>
				<li class="overview">
					<a href="index.php">Home</a>
				</li>
				<li class="has-children notifications active">
					<a href="#0">My Internships</a>
					<ul>
						<li class="active"><a href="#">Applied</a></li>
						<li><a href="Myinternships_selected.php">Selected</a></li>
					</ul>
				</li>
				<li class="comments">
					<a href="Mybookmarks.php">My BookMarks</a>
				</li>
			</ul>
			<ul>
				<li class="bookmarks">
					<a href="eventdetails.php">Event Details</a>
				</li>
				<li class="images">
					<a href="importantdates.php">Important Dates</a>
				</li>
			</ul>
		</nav>
		<div class="content-wrapper">
			
			<?php
				$result = $conn->query("Select * from intern WHERE id in (select intern_id from applied where user_id = $loggedid)");
					if ($result->num_rows > 0) {
					    while ($row = $result->fetch_array()) {
			?>			<section class="hide">
							<div class="dash-unit">
								<h4><?php echo $row["name"]; ?></h4>
								<div class="circle"><p><?php $string = $row["name"];
									echo strtoupper($string[0]);?></p></div>
								<?php $links = "http://" . $row['link'] ?>
									<h3><a href="<?php echo $links ?>">Website</a><h3>

								<p><?php echo $row["city"]; ?></p>
								<div id="field">
                                   <p><?php echo $row['fields']; ?></p>
								</div>
								
							</div>
							<div class="modal">
							<div class="modal-content" id="modal">
								<div class="modal-header">
									<a href="Myinternships_applied.php" class="close">x</a>
									<h2><?php echo $row["name"]; ?></h2>
							</div>
								<h3>About</h3>
								<p><?php echo $row["about"]; ?></p>
								<h3>Website</h3>
								<p><a href="<?php echo $links ?>"><?php echo $links ?></a></p>
								<h3>Location</h3>
       							<p><?php echo $row["location"]; ?></p>
       							<h3>Fields</h3>
								<p><?php echo $row["fields"]; ?></p>
								<h3>Stipend</h3>
								<p><?php echo $row["stipend"]; ?></p>
								<h3>Duration</h3>
								<p><?php echo $row["duration"]; ?></p>
                                <h3>Visit Date</h3>
								<p><?php echo $row["visitDate"]; ?></p>
								<h3>Youtube </h3>								
								<p><a href="<?php echo $row['YoutubeLink']; ?>"><?php echo $row['YoutubeLink']; ?></a></p>
								<?php 
									$internid = $row["id"];
									$comName = $row["name"]; 
									$timeresult = $conn->query("SELECT appliedAt FROM applied WHERE (user_id = $loggedid AND intern_id = $internid)");
									$timerow = $timeresult ->fetch_assoc();							
									$dt = new DateTime();
									$dt->setTimezone(new DateTimeZone('Asia/Calcutta'));
									$currTime = ($dt->format('Y-m-d H:i:s'));
									$datetime1 = new DateTime($timerow['appliedAt']);
									$datetime2 = new DateTime($currTime);
									$interval = $datetime1->diff($datetime2);
									$day = $interval->format('%a');
									$hours = $interval->format('%h');
									$tothr = ($day * 24) + $hours;
									if($tothr <= 12){
								?>
		
								<form action="cancel_bind.php" method="POST">
									<input type="hidden" name="comId" value="<?php echo$conn->real_escape_string($internid); ?>" />
									<input type="hidden" name="userId" value="<?php echo$conn->real_escape_string($loggedid); ?>" />
									 <div class="modal-footer"> 									
										<input type="submit" value="Cancel"/>
  									</div>
								</form>
								<form action="bookmarks.php" method="POST">
									<input type="hidden" name="comId" value="<?php echo$conn->real_escape_string($internid); ?>" />
									<input type="hidden" name="userId" value="<?php echo$conn->real_escape_string($loggedid); ?>" />
									 <div class="modal-footer">
										<input type="submit" class="book_btn" value="Bookmark"/>
  									</div>
								</form>
								<?php
									}//end of if
								?>
							</div>
						</div>
						</section>	
			<?php
					    }
					} else {
						echo "<br/>No Internships Applied!";
					}
			?>
			<div class="note">
		<h3>Company selected will be locked after 12 hours, if you want to alter your choice please do it within 12 hours of your selection.</h3></div>
		</div> 
	</main> 
<script src="js/jquery-2.1.4.js"></script>
<script src="js/jquery.menu-aim.js"></script>
<script src="js/main.js"></script> 
<script>
	$result = $('section');
	for($j = 0; $j < $result.length; $j++){
		$result[$j].addEventListener("click", function(){
			this.className = 'show';
		});
	}
	function logout(){
		window.location="/dashboard/loginpage.php";
	}
	
</script>
</body>
</html>