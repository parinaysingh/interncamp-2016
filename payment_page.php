<?php
	session_start();
	require_once('db_config.php');
	if(!isset($_SESSION["paymentid"])){
		header('Location: /dashboard/loginpage.php');
		exit();
	}
	$paymentid = $_SESSION['paymentid'];
	if($stmt = $conn->prepare("SELECT payid FROM internapply WHERE id = ?")){			
		$stmt->bind_param('i',$paymentid);	
		$stmt->execute();
		$stmt->store_result();
		$stmt->bind_result($pid);
		$stmt->fetch();
		if($pid){
			$_SESSION['cvlogged'] = $paymentid;
			header('Location: /dashboard/cvuploadpage.php');
			exit();
		}
	}else{ 
		echo "Error";
		exit();
	}	
?>
<!doctype html>
<html>
<head>
	<link rel="icon" href="img/icamp.png">
	<title>Payment Gateway</title>
	<style>
	.pay img{
		margin-left: 40%;
	}
		</style>
	<body>
		
		<a href="http://interncamp.ecell.org.in/dashboard/regclose.php" class="pay"><img src="img/Pay.gif" ></a>
		<br>
		<div style ='font:25px Papyrus,fantasy'><p align=center>Please Complete The Payment!</p> <h2 align=center style="color: red;">Your Account will be updated within 24 hours after payment</h2></div><br><br><br>
		
	</body>
</html>
