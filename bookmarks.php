<!doctype html>
<html lang="en">
	<head>
		<link rel="icon" href="img/icamp.png">
		<meta charset="utf-8">
		<title>Redirect with countdown</title>
		<style>
			html { 
			  background: url(img/loading.gif) no-repeat center center fixed; 
			  -webkit-background-size: cover;
			  -moz-background-size: cover;
			  -o-background-size: cover;
			  background-size: cover;
			  color: white;
			}
		</style>
	</head>
	<body>
		<script type="text/javascript">
			 (function () {
			  var timeLeft = 3,
			  cinterval;
			  var timeDec = function (){
			  timeLeft--;
			  document.getElementById('countdown').innerHTML = timeLeft;
			  if(timeLeft === 0){
			  clearInterval(cinterval);
				}
			};
			cinterval = setInterval(timeDec, 1000);
			})();
			function Redirect() 
			{  
				window.location="index.php"; 
			}  
			setTimeout('Redirect()', 3000);
		</script>
		Redirecting in <span id="countdown">3</span> seconds.
		<br><br>
		<?php		 
		session_start();
			require_once('db_config.php');
					
			if(!isset($_SESSION["loggedid"])){
				header('Location: /dashboard/loginpage.php');
				exit();
			}
		if (isset($_POST['userId']) && isset($_POST['comId'])) {
			$userId = $conn->real_escape_string($_POST['userId']);
			$comId = $conn->real_escape_string($_POST['comId']);			
			$query2 = "Select user_id, intern_id from Bookmarks";
			if($stmt = $conn->prepare($query2)){
				$stmt->execute();
				$stmt->store_result();
				$stmt->bind_result($uid, $iid);
				while ($stmt->fetch()) {
					if($userId==$uid AND ($comId==$iid)){
						echo "<div style ='font:50px Papyrus,fantasy'><p align=center>Already Bookmarked</p></div>";
						header('Refresh: 3;Mybookmarks.php');
						exit();
					}
				}
				$stmt->free_result();			
			}				
			$insert="INSERT INTO Bookmarks(user_id, intern_id) VALUES(?, ?)";
			if($stmt=$conn->prepare($insert)){
					$stmt->bind_param("ii",$userId, $comId);
					$stmt->execute();
					$stmt->close();
					echo "<div style ='font:50px Papyrus,fantasy'><p align=center>Successfully Bookmarked</p></div>";
			  header('Refresh: 3;Mybookmarks.php');
				}else{
					echo "<div style ='font:50px Papyrus,fantasy'><p align=center>Error! Please Try Again</p></div>";
			  header('Refresh: 3;Mybookmarks.php');
					var_dump($conn->error);
				}
		}else{
			echo "<div style ='font:50px Papyrus,fantasy'><p align=center>Required Field Missing</p></div>";
			header('Refresh: 3;Mybookmarks.php');
		}
		?>
	</body>
</html>