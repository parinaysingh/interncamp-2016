<!doctype html>
<html lang="en">
<head>
  <link rel="icon" href="img/icamp.png">
<meta charset="utf-8">
<title>Redirect with countdown</title>
<style>
html { 
  background: url(img/loading.gif) no-repeat center center fixed; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
  color: white;
}
</style>
</head>
<body>
<script type="text/javascript">

 (function () {
  var timeLeft = 3,
  cinterval;

  var timeDec = function (){
  timeLeft--;
  document.getElementById('countdown').innerHTML = timeLeft;
  if(timeLeft === 0){
  clearInterval(cinterval);
    }
};

cinterval = setInterval(timeDec, 1000);
})();
function Redirect() 
{  
window.location="Myinternships_applied.php"; 
} 
setTimeout('Redirect()', 3000);

</script>
Redirecting in <span id="countdown">3</span> seconds to the Applied Page.
<br><br>
<?php
 
session_start();
	require_once('db_config.php');
			
	if(!isset($_SESSION["loggedid"])){
		header('Location: /dashboard/loginpage.php');
		exit();
	}
 
// check for required fields
if (isset($_POST['userId']) && isset($_POST['comId'])) {
 
    $userId = $conn->real_escape_string($_POST['userId']);
    $comId = $conn->real_escape_string($_POST['comId']);
	
	
	$result = $conn->query("Select applicants, maxapplicants FROM intern WHERE id = $comId");
	$row = $result->fetch_array();
	$applicants = $row['applicants'];
	if($applicants){
		$applicants--;
	}else{
		$applicants = 0;
	}
	$insert="DELETE FROM applied WHERE user_id = ? and intern_id = ?";
    	if($stmt=$conn->prepare($insert)){
    		$stmt->bind_param("ii",$userId, $comId);
    		$stmt->execute();
    		$stmt->close();
			$insert="UPDATE intern SET applicants = ? WHERE id = ?";
			
			if($stmt=$conn->prepare($insert)){
				$stmt->bind_param("ii",$applicants, $comId);
				$stmt->execute();
				$stmt->close();
			}else{
				var_dump($conn->error);
			}
			echo "<div style ='font:50px Papyrus,fantasy'><p align=center>Successfully Cancelled</p></div>";
			header('Refresh: 3;Myinternships_applied.php');
    	}else{
			echo "<div style ='font:50px Papyrus,fantasy'><p align=center>Error! Not Cancelled</p></div>";
			header('Refresh: 3;Myinternships_applied.php');
    		var_dump($conn->error);
    	}
}else{
	echo "<div style ='font:50px Papyrus,fantasy'><p align=center>Error Try Again</p></div>";
	header('Refresh: 3;Myinternships_applied.php');
}
?>
</body>
</html>