<!doctype html>
<?php 
	session_start();
	require_once('db_config.php');
			
	if(!isset($_SESSION["loggedid"])){
		header('Location: /dashboard/loginpage.php');
		exit();
	}
	$loggedid = $_SESSION["loggedid"];
	
?>
<html lang="en" class="no-js">
<head>
	<link rel="icon" href="img/icamp.png">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="css/reset.css"> 
	<link rel="stylesheet" href="css/style.css"> 
	<script src="js/modernizr.js"></script> 
	 <link rel="stylesheet" href="css/main.css"> 	
	<title>Dashboard</title>
	<style>
		tr:hover{background-color:black;}
		table, th, td {
	 	  border: 3px ridge #00838F;
		 color: #fff;
		}
		table{
		margin-top :30px;
		}
		th{
		    background-color:rgb(28,31,34);
		    padding:20px;
		}
		td{   
		    background-color:rgb(0,131,143); 
		    opacity:0.7;   
		    padding:15px;
		}
		.name{
			width: 35%;
		}
		.visitDate{
			width:10%;
		}
		.interviewTime{
			width:20%;
		}
		.venue{
			width: 35%;
		}
		.tableheader img{
			width:100%;
		}
		.tableheader{
		     padding:3em 0 0 0;
		     width: 120px;
		     height:auto;
		     margin: 0 auto;
		    
		}
	</style>
</head>
<body>
	<header class="cd-main-header">
		<a href="#0" class="cd-logo"><img src="img/icamp.png" alt="Logo" height="65px" width="90px"></a>

		<a href="#0" class="cd-nav-trigger">Menu<span></span></a>

		<nav class="cd-nav">
			<ul class="cd-top-nav">
				
				<li class="has-children account">
					<a href="#0">
						
						Account
					</a>

					<ul>
						<li><a href="#0" onClick="logout()">Logout</a></li>
					</ul>
				</li>
			</ul>
		</nav>
	</header> 
	<main class="cd-main-content">
		<nav class="cd-side-nav">
			<ul>
				<li class="has-children overview ">
					<a href="index.php">Home</a>	
				</li>
				<li class="has-children notifications">
					<a href="#0">My Internships</a>
					<ul>
						<li><a href="Myinternships_applied.php">Applied</a></li>
						<li><a href="Myinternships_selected.php">Selected</a></li>
					</ul>
				</li>
				<li class="has-children comments">
					<a href="Mybookmarks.php">My BookMarks</a>
				</li>
			</ul>
			<ul>
				<li class="has-children bookmarks active">
					<a href="#0">Event Details</a>	
				</li>
				<li class="has-children images">
					<a href="importantdates.php">Important Dates</a>
				</li>
			</ul>
		</nav>
		<div class="content-wrapper">
			<div  class = "tableheader">
			<img src = "img/c2.png">
			</div>
			<?php
					$result = $conn->query("Select name, visitDate, interviewTime, venueDetails from intern WHERE visitDate = '2016-03-17'");
					if ($result->num_rows > 0) {
					    ?> <table border="1" style="width:100%"> 
							<tr>
								<th class = "name">Name </th>
								<th class = "visitDate" >Visit Date </th>
								<th class = "interviewTime">Interview Time </th>
								<th class = "venue">Venue Details </th>
							</tr>
						<?php
					    while ($row = $result->fetch_assoc()) {						
			?>		
						<tr>
							<td><?php echo $row['name'] ?></td>
							<td><?php echo $row['visitDate'] ?></td>
							<td><?php echo $row['interviewTime'] ?></td>
							<td><?php echo $row['venueDetails'] ?></td>
						</tr>	
                       						
			<?php			
						}//end of while
			?> 			</table> 
			
			<?php 
					
					} else {
						echo "No Results Selected";
					}
			?>
			
			<div  class = "tableheader">
			<img src = "img/c3.png">
			</div>
			
			<?php
					$result = $conn->query("Select name, visitDate, interviewTime, venueDetails from intern WHERE visitDate = '2016-03-18'");
					if ($result->num_rows > 0) {
					    ?> <table border="1" style="width:100%"> 
							<tr>
								<th class = "name">Name </th>
								<th class = "visitDate" >Visit Date </th>
								<th class = "interviewTime">Interview Time </th>
								<th class = "venue" >Venue Details </th>
							</tr>
						<?php
					    while ($row = $result->fetch_assoc()) {						
			?>		
						<tr>
							<td><?php echo $row['name'] ?></td>
							<td><?php echo $row['visitDate'] ?></td>
							<td><?php echo $row['interviewTime'] ?></td>
							<td><?php echo $row['venueDetails'] ?></td>
						</tr>	
                       						
			<?php			
						}//end of while
			?> 			</table> 
			
			<?php 
					
					} else {
						echo "No Results Selected";
					}
			?>
			
			<div  class = "tableheader">
			<img src = "img/c4.png">
			</div>
			
			<?php
					$result = $conn->query("Select name, visitDate, interviewTime, venueDetails from intern WHERE visitDate = '2016-03-19'");
					if ($result->num_rows > 0) {
					    ?> <table border="1" style="width:100%"> 
							<tr>
								<th class = "name">Name </th>
								<th class = "visitDate" >Visit Date </th>
								<th class = "interviewTime">Interview Time </th>
								<th class = "venue">Venue Details </th>
							</tr>
						<?php
					    while ($row = $result->fetch_assoc()) {						
			?>		
						<tr>
							<td><?php echo $row['name'] ?></td>
							<td><?php echo $row['visitDate'] ?></td>
							<td><?php echo $row['interviewTime'] ?></td>
							<td><?php echo $row['venueDetails'] ?></td>
						</tr>	
                       						
			<?php			
						}//end of while
			?> 			</table> 
			
			<?php 
					
					} else {
						echo "No Results Selected";
					}
			?>
			
			<div  class = "tableheader">
			<img src = "img/c5.png">
			</div>
			
			<?php
					$result = $conn->query("Select name, visitDate, interviewTime, venueDetails from intern WHERE visitDate = '2016-03-20'");
					if ($result->num_rows > 0) {
					    ?> <table border="1" style="width:100%"> 
							<tr>
								<th class = "name">Name </th>
								<th class = "visitDate" >Visit Date </th>
								<th class = "interviewTime">Interview Time </th>
								<th class = "venue">Venue Details </th>
							</tr>
						<?php
					    while ($row = $result->fetch_assoc()) {						
			?>		
						<tr>
							<td><?php echo $row['name'] ?></td>
							<td><?php echo $row['visitDate'] ?></td>
							<td><?php echo $row['interviewTime'] ?></td>
							<td><?php echo $row['venueDetails'] ?></td>
						</tr>	
                       						
			<?php			
						}//end of while
			?> 			</table> 
			
			<?php 
					
					} else {
						echo "No Results Selected";
					}
			?>
		</div> 
	</main> 
<script src="js/jquery-2.1.4.js"></script>
<script src="js/jquery.menu-aim.js"></script>
<script src="js/main.js"></script> 
<script>
	$result = $('section');
	for($j = 0; $j < $result.length; $j++){
		$result[$j].addEventListener("click", function(){
			this.className = 'show';
		});
	}
	function logout(){
		window.location="/dashboard/loginpage.php";
	}
	
</script>
</body>
</html>