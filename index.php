<!doctype html>
<?php 
	session_start();
	require_once('db_config.php');
			
	if(!isset($_SESSION["loggedid"])){
		header('Location: /dashboard/loginpage.php');
		exit();
	}
	
	$loggedid = $_SESSION["loggedid"];
	$usercategory = $conn->query("SELECT name, category FROM internapply WHERE id = $loggedid");
	$usercategory = $usercategory->fetch_array();
	$username = $usercategory['name'];
	$usercategory = $usercategory['category'];
	
	
?>
<html lang="en" class="no-js">
<head>
	<link rel="icon" href="img/icamp.png">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="css/reset.css"> 
	<link rel="stylesheet" href="css/style.css"> 
	<script src="js/modernizr.js"></script> 
	 <link rel="stylesheet" href="css/main.css"> 	
	<title>Dashboard</title>
</head>
<body>
	<header class="cd-main-header">
		<a href="#0" class="cd-logo"><img src="img/icamp.png" alt="Logo" height="65px" width="90px"></a>
		<a href="#0" class="cd-nav-trigger">Menu<span></span></a>
		<nav class="cd-nav">
			<ul class="cd-top-nav">
				<li class="has-children account">
					<a href="#0">
						Account
					</a>
					<ul>
						<li><a href="#0" onClick="logout()">Logout</a></li>
					</ul>
				</li>
			</ul>
		</nav>
	</header>
	<main class="cd-main-content">
		<nav class="cd-side-nav">
			<ul>
				<li class="overview active">
					<a href="#0">Home</a>		
				</li>
				<li class="has-children notifications">
					<a href="#0">My Internships</a>
					<ul>
						<li><a href="Myinternships_applied.php">Applied</a></li>
						<li><a href="Myinternships_selected.php">Selected</a></li>
					</ul>
				</li>
				<li class="comments">
					<a href="Mybookmarks.php">My BookMarks</a>
				</li>
			</ul>
			<ul>
				<li class="bookmarks">
					<a href="eventdetails.php">Event Details</a>
				</li>
				<li class="images">
					<a href="importantdates.php">Important Dates</a>
				
				</li>
			</ul>
		</nav>
		<div class="content-wrapper">		
			<?php
				$query2 = "Select id, name, about, link, location, city, fields, stipend, duration, category, applicants, maxapplicants, visitDate, youtubeLink FROM intern";
				if($stmt = $conn->prepare($query2)){
					$stmt->execute();
					$stmt->store_result();
					$num_of_rows = $stmt->num_rows;
		
					$stmt->bind_result($ID, $name, $about, $link, $location, $city, $fields, $stipend, $duration, $category, $applicants, $maxapplicants, $visitDate, $youtubeLink);			
			   }else{
					echo "Error";
					exit();
			   }
					if ($num_of_rows > 0) {
					    while ($stmt->fetch()) {
							if($usercategory >= $category){
			?>			
							<section class="hide">
								<div class="dash-unit">
									<h4><?php echo $name; ?></h4>
								<div class="circle"><p><?php $string = $name;
										echo strtoupper($string[0]);?></p></div>
									<?php $links = "http://" . $link ?>
									<h3><a href="<?php echo $links ?>">Website</a><h3>
									<h3><?php echo $city; ?></h3>
									<div id="field">
                                        <h3><?php echo $fields; ?></h3>
									</div>
								</div>
								<div class="modal">
								<div class="modal-content" id="modal">
									<div class="modal-header">
										<a href="index.php" class="close">x</a>
										<h2><?php echo $name; ?></h2>
								</div>
									<h3>About</h3>
									<p><?php echo $about; ?></p>
									<h3>Website</h3>
									<p><a href="<?php echo $links ?>"><?php echo $links ?></a></p>
									<h3>Location</h3>
									<p><?php echo $location; ?></p>
									<h3>Fields</h3>
									<p><?php echo $fields; ?></p>
									<h3>Stipend</h3>
									<p><?php echo $stipend; ?></p>
									<h3>Duration</h3>
									<p><?php echo $duration; ?></p>
									<h3>Visit Date</h3>
									<p><?php echo $visitDate; ?></p>
									<h3>Youtube </h3>								
									<p><a href="<?php echo $youtubeLink ?>"><?php echo $youtubeLink ?></a></p>
									<?php 
										$diff = $maxapplicants - $applicants;
									if($diff < 11){
									?>
									 <h3>Vacancies</h3>
                                                                         <p><?php echo $diff ?></p>
									<?php }
										$internid = $ID;
										$comName = $name; 
									?>
									<h3>Note: Company selected will be locked after 12 hours, if you want to alter your choice please do it within 12 hours of your selection.</h3><br>
									<form action="apply_bind.php" method="POST">
										<input type="hidden" name="comId" value="<?php echo $internid; ?>" />
										<input type="hidden" name="userId" value="<?php echo $loggedid; ?>" />
										<input type="hidden" name="comName" value="<?php echo $comName; ?>" />
										<input type="hidden" name="userName" value="<?php echo $username; ?>" />
										 <div class="modal-footer">		
										<input type="submit" value="Apply"/>
										</div>
									</form>
									<form action="bookmarks.php" method="POST">
										<input type="hidden" name="comId" value="<?php echo $internid; ?>" />
										<input type="hidden" name="userId" value="<?php echo $loggedid; ?>" />
										<div class="modal-footer">
											
										<input type="submit" class="book_btn" value="Bookmark"/>
										</div>
									</form>
								</div>
							</div>
							</section>	
			<?php
							}
						}
					} else {
						echo "No Results Selected";
					}
			?>

		</div> 
	</main> 
	<script src="js/jquery-2.1.4.js"></script>
	<script src="js/jquery.menu-aim.js"></script>
	<script src="js/main.js"></script> 
	<script>
		$result = $('section');
		for($j = 0; $j < $result.length; $j++){
			$result[$j].addEventListener("click", function(){
				this.className = 'show';
			});
		}
		function logout(){
			window.location="/dashboard/loginpage.php";
		}
		
	</script>
</body>
</html>