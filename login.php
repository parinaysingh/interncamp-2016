<?php
session_start();
require_once('db_config.php');

if (isset($_POST['email']) && isset($_POST['password'])){
	$mail = $conn->real_escape_string($_POST['email']);
	$pass = $conn->real_escape_string($_POST['password']);
	if($stmt = $conn->prepare("SELECT id, password FROM internapply WHERE mail = ?")){
		$stmt->bind_param('s',$mail);	
		$stmt->execute();
		$stmt->store_result();
		$stmt->bind_result($id, $password);
		$stmt->fetch();
		if(password_verify($pass, $password)){
			$_SESSION["paymentid"] = $id;
			header('Location: /dashboard/payment_page.php');
			exit();
		}
    }
   $stmt->free_result();
   $stmt->close();
	}
	echo "Email or Password incorrect";
?>
