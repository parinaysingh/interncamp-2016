<?php

session_start();
require_once('db_config.php');
	
if(isset($_FILES['file'])){
	$cvlogged = $_SESSION['cvlogged'];
	if($stmt = $conn->prepare("SELECT name, password FROM internapply WHERE id = ?")){
	$stmt->bind_param('i',$cvlogged);	
    $stmt->execute();
    $stmt->store_result();
	$stmt->bind_result($name, $password);
    $stmt->fetch();

		$file = $_FILES['file'];
		$file_name = $file['name'];
		$file_size = $file['size'];
		$file_error = $file['error'];
		$file_ext = explode('.', $file_name);
		$file_ext = strtolower(end($file_ext));
		$file_tmp = $file['tmp_name'];
		print_r($file_ext);
		$allowed = array('txt','pdf','jpg','png','docx');
		if(in_array($file_ext, $allowed)){
			if($file['error'] === 0){                               
				$file_destination = 'studentCV/' . $name . '.' . $file_ext;
				if($file_size < 2097152){
					if(move_uploaded_file($file_tmp, $file_destination)){	
						$insert="UPDATE internapply SET cvid = ? WHERE id = ?";
						$bool = 1;
						if($stmt=$conn->prepare($insert)){
								$stmt->bind_param("ii", $bool, $cvlogged);
								$stmt->execute();
								$stmt->close();
								header('Location: /dashboard/passwordupdatepage.php');
			                    exit();
							}else{
								echo "Error! Please Try Again";
								var_dump($conn->error);
							}			
					}
				}else echo "Please Upload Files Less than 2mb ";
			}else echo "File Error ";
		}else echo "Please Upload .txt, .jpg, .png, .pdf, .docx Files Only";
	}else echo "Error";
}else{
	echo "Required Fields Missing";
}
?>

