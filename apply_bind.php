<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<link rel="icon" href="img/icamp.png">
		<title>Redirect with countdown</title>
		<style>
			html { 
			  background: url(img/loading.gif) no-repeat center center fixed; 
			  -webkit-background-size: cover;
			  -moz-background-size: cover;
			  -o-background-size: cover;
			  background-size: cover;
			  color: white;
			}
		</style>
	</head>
	<body>
		<script type="text/javascript">
			 (function () {
			  var timeLeft = 3,
			  cinterval;
			  var timeDec = function (){
			  timeLeft--;
			  document.getElementById('countdown').innerHTML = timeLeft;
			  if(timeLeft === 0){
			  clearInterval(cinterval);
				}
			};

			cinterval = setInterval(timeDec, 1000);
			})();
			function Redirect() 
			{  
				window.location="index.php"; 
			}  
			setTimeout('Redirect()', 3000);
		</script>
		Redirecting in <span id="countdown">3</span> seconds to the Homepage.
		<br><br>
		<?php
		 
		session_start();
			require_once('db_config.php');					
			if(!isset($_SESSION["loggedid"])){
				header('Location: /dashboard/loginpage.php');
				exit();
			}
		if (isset($_POST['userId']) && isset($_POST['comId']) && isset($_POST['comName']) && isset($_POST['userName'])) {
			$userId = $conn->real_escape_string($_POST['userId']);
			$comId = $conn->real_escape_string($_POST['comId']);
			$comName = $conn->real_escape_string($_POST['comName']);
			$userName = $conn->real_escape_string($_POST['userName']);
			$query2 = "Select id from applied where user_id = ?"; 
			if($stmt = $conn->prepare($query2)){
				$stmt->bind_param('i',$userId);
				$stmt->execute();
				$stmt->store_result();
				$num_of_rows = $stmt->num_rows;
				if($num_of_rows > 3){
					echo "<div style ='font:50px Papyrus,fantasy'><p align=center>Maximum limit for application reached<p></div>";
					header('Refresh: 3;index.php');
					exit();
				}
				$stmt->free_result();
			}else{
				echo "Error 1";
			}
			$query2 = "Select user_id, intern_id from applied";
			if($stmt = $conn->prepare($query2)){
			$stmt->execute();
			$stmt->store_result();
			$stmt->bind_result($uid, $iid);
			while ($stmt->fetch()) {
				if($userId==$uid AND ($comId==$iid)){
					?> 
					<div style ='font:50px Papyrus,fantasy'><p align=center>Already Applied</p></div>
					<?php
					header('Refresh: 3;index.php');
					exit();				
				}
			}
				$stmt->free_result();
			}
			$result = $conn->query("Select applicants, maxapplicants FROM intern WHERE id = $comId");
			$row = $result->fetch_array();
			$applicants = $row['applicants'];
			$maxapplicants = $row['maxapplicants'];			
			if($applicants == $maxapplicants){
				?>
				<div style ='font:50px Papyrus,fantasy'><p align=center>No Vacany! Please Try Again Later.</p></div>
				<?php
				header("refresh:3; url=index.php");						
				exit();
			}
			$applicants++;
			$dt = new DateTime();
			$dt->setTimezone(new DateTimeZone('Asia/Calcutta'));
			$currTime = ($dt->format('Y-m-d H:i:s'));
			$insert="INSERT INTO applied(user_id, intern_id, userName, appliedFor, appliedAt) VALUES(?, ?, ?, ?, ?)";
			if($stmt=$conn->prepare($insert)){
					$stmt->bind_param("iisss",$userId, $comId, $userName, $comName, $currTime);
					$stmt->execute();
					$stmt->close();
					$insert="UPDATE intern SET applicants = ? WHERE id = ?";
					if($stmt=$conn->prepare($insert)){
						$stmt->bind_param("ii",$applicants, $comId);
						$stmt->execute();
						$stmt->close();
					}else{
						var_dump($conn->error);
					}
					?>
					<div style ='font:50px Papyrus,fantasy'><p align=center>Successfully Applied</p></div>
					<?php
					header('Refresh: 3;index.php');					
				}else{
					?>
					<div style ='font:50px Papyrus,fantasy'><p align=center>Error! Please Try Again</p></div>
					<?php
					header('Refresh: 3;index.php');
					var_dump($conn->error);
				}
			}else{
				?>
				<div style ='font:50px Papyrus,fantasy'><p align=center>Required Field Missing</p></div>";
				<?php
				header('Refresh: 3;index.php');
			}
		?>
	</body>
</html>