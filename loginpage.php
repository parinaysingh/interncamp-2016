<!DOCTYPE html>
 <html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" href="img/icamp.png">
<title>Login | Internship Camp</title>
<style type="text/css">
body {
background-image: url(img/loginpage.jpg);
background-size: 100%;
;
font-family: 'Open Sans', Arial, Helvetica, sans-serif;
font-size: 16px;
line-height: 1.5em;
}
a { text-decoration: none; }
h1 { font-size: 1em;
color: white; }
h1, p {
margin-bottom: 10px;
}
strong {
font-weight: bold;
}
.uppercase { text-transform: uppercase; }

/* ---------- LOGIN ---------- */
#login {
margin: 50px auto;
width: 300px;
}
form fieldset input[type="text"], input[type="password"] {
background-color: #ffffff;
border: none;
border-radius: 3px;
-moz-border-radius: 3px;
-webkit-border-radius: 3px;
color: #009688;
font-family: 'Open Sans', Arial, Helvetica, sans-serif;
font-size: 14px;
height: 50px;
outline: none;
padding: 0px 10px;
width: 280px;
-webkit-appearance:none;
}
form fieldset input[type="submit"] {
background-color: #ff5722;
border: none;
border-radius: 3px;
-moz-border-radius: 3px;
-webkit-border-radius: 3px;
color: #f4f4f4;
cursor: pointer;
font-family: 'Open Sans', Arial, Helvetica, sans-serif;
text-size: 18px;
height: 50px;
text-transform: uppercase;
width: 300px;
-webkit-appearance:none;
}
form fieldset a {
color: #ffffff;
font-size: 10px;
}
form fieldset a:hover { text-decoration: underline; }
.forgot p{
text-align: right;

}
.dash{
			background-color: #00838f;
			padding:10px;
			width: 320px;
			height: 70px;
			margin:26px 18px 0px 0px;
			border-radius: 0px;
			float: left;
			border: 1px solid #009688;
			color: black;
			opacity: 1;
			margin-top: 12px;
			-moz-box-shadow:    0px 3.4px 2.2p 0px #ffffff;
    -webkit-box-shadow: 0px 3.4px 2px 0px #ffffff;
    box-shadow:         0px 3.4px 2px 0px #ffffff;
				
			 }
.dash p{
color: #ffffff;
margin-top: 2px;
text-align: center;
}


</style>
</head>
<body>
	<?php
			session_start();
			session_unset(); 
			session_destroy();
		?>
<div class="dash">
<p>Entrepreneurship Cell<br>
Internship Camp 2016<br>
Imagine | Innovate | Implement</p>
</div>
<div id="login">

<h1><strong>Welcome.</strong> Please login.<img src="img/icamp.png" alt="Logo" height="70px" width="90px"></h1>
<form action="login.php" method="POST">
<fieldset>
<p><input type="text" required Placeholder="Username" name="email"></p>
<p><input type="password" required Placeholder="Password" name="password"></p>
<div class="forgot">
<p><a href="#"><strong>Forgot Password?<strong></a></p>
</div>
<p><input type="submit" value="Login"></p>
</fieldset>
</form>
</div> 
</body>
</html>