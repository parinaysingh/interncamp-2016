# README #

A KIIT E-Cell Initiative, KIIT University Bhubaneswar 

The Internship Camp is an initiative of KIIT Entrepreneurship Cell which aims to provide internship opportunities to undergraduate students from the field of technology, biotechnology, management, design, and law.
The Internship Camp also provides a networking platform to all the start-ups and corporate firms for exchanging ideas and finding the right talent.



![](previewImages/home.JPG)
![](previewImages/home2.JPG)
![](previewImages/evdetails.JPG)
![](previewImages/mob_home.JPG)