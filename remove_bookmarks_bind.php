<!doctype html>
<html lang="en">
	<head>
		<link rel="icon" href="img/icamp.png">
		<meta charset="utf-8">
		<title>Redirect with countdown</title>
		<style>
		html { 
		  background: url(img/loading.gif) no-repeat center center fixed; 
		  -webkit-background-size: cover;
		  -moz-background-size: cover;
		  -o-background-size: cover;
		  background-size: cover;
		  color: white;
		}
		</style>
	</head>
	<body>
		<script type="text/javascript">
			 (function () {
			  var timeLeft = 3,
			  cinterval;

			  var timeDec = function (){
			  timeLeft--;
			  document.getElementById('countdown').innerHTML = timeLeft;
			  if(timeLeft === 0){
			  clearInterval(cinterval);
				}
			};

			cinterval = setInterval(timeDec, 1000);
			})();
			function Redirect() 
			{  
			window.location="Mybookmarks.php"; 
			} 
			setTimeout('Redirect()', 3000);
		</script>
		Redirecting in <span id="countdown">3</span> seconds.
		<br><br>
		<?php		 
		require_once('db_config.php');
		if (isset($_POST['userId']) && isset($_POST['comId'])) {
			$userId =$conn->real_escape_string($_POST['userId']);
			$comId =$conn->real_escape_string($_POST['comId']);
			$insert="DELETE FROM Bookmarks WHERE user_id = ? and intern_id = ?";
				if($stmt=$conn->prepare($insert)){
					$stmt->bind_param("ii",$userId, $comId);
					$stmt->execute();
					$stmt->close();
					echo "<div style ='font:50px Papyrus,fantasy'><p align=center>Bookmark Removed</p></div>";
					header('Refresh: 3; url=Mybookmarks.php');
				}else{
					echo "<div style ='font:50px Papyrus,fantasy'><p align=center>Bookmark Not Removed</p></div>";
					header('Refresh: 3; url=Mybookmarks.php');
					var_dump($conn->error);
				}
		}else{
			echo "<div style ='font:50px Papyrus,fantasy'><p align=center>Error Try Again</p></div>";
			header('Refresh: 3; url=Mybookmarks.php');
		}
		?>
	</body>
</html>